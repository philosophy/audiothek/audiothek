<?php

/*
 * Plugin Name: Audiothek
 * Version: 0.1.0
 * Description: <a href='https://wordpress.org/plugins/seriously-simple-podcasting'>Seriously Simple Podcasting</a> extension.
 * Author: Odin Kroeger
 * Author URI: mailto:odin.kroeger@univie.ac.at
 * Requires PHP: 8.2
 * Text Domain: audiothek
 * Domain Path: /lang
 */

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Constants
 */

/* Base directory of the plugin. */
define(__NAMESPACE__ . '\\BASE_DIR', plugin_dir_path(__FILE__));

/* Base URL of the plugin. */
define(__NAMESPACE__ . '\\BASE_URL', plugin_dir_url(__FILE__));

/* Plugin file. */
define(__NAMESPACE__ . '\\PLUGIN_FILE', plugin_basename(__FILE__));

/* Base directory for PHP modules. */
const PHP_DIR = BASE_DIR . 'php' . DIRECTORY_SEPARATOR;

/* Directory to load other PHP files from. */
const INCLUDE_DIR = PHP_DIR . 'includes' . DIRECTORY_SEPARATOR;

/* Directory to auto-load classes from. */
const CLASS_DIR = PHP_DIR . 'classes';


/*
 * Modules
 */

require INCLUDE_DIR . 'autoloader.php';
require INCLUDE_DIR . 'plugin.php';


/*
 * Main
 */

/* FIXME: The autoloader should only be registered if the Plugin is active */
Autoloader\register(namespace: __NAMESPACE__, basedir: CLASS_DIR);

register_activation_hook(__FILE__, __NAMESPACE__ . '\\Plugin\\activate');

add_action('init', __NAMESPACE__ . '\\Plugin\\init');
