.POSIX:

#
# Macros
#

CTAGS = ctags
DESTDIR =
GPG = gpg -ab
GPGFLAGS = -q --batch --yes
INSTALL = install
MSGFMT = msgfmt
PATHCHK = pathchk
PHPCSFIXER = php-cs-fixer
PHPCSFIXERFLAGS = --rules=@PER-CS1.0
PHPSTAN = phpstan
SHELL = /bin/sh
TAR = pax -ws '/^/$(distname)\//' -x ustar
UNTAR = tar x
WP = wp
GZIP = gzip
ZIP = zip

plugin = $(package)
pluginar = $(plugin).zip
pluginfiles = \
	$(scripts) \
	css/admin.css \
	js/post-new.js \
	lang/de_DE.mo \
	lang/de_DE.po \
	lang/audiothek.pot

distname = $(package)-$(version)
distar = $(distname).tgz
distfiles = \
	$(pluginfiles) \
	Makefile \
	docs/style.rst

langdomain = audiothek
langdir = lang
langfiles = $(langdir)/de_DE.mo
package = audiothek

scripts = \
	audiothek.php \
	php/classes/course.php \
	php/classes/episode.php \
	php/classes/errors/abc.php \
	php/classes/errors/date-conv.php \
	php/classes/errors/init.php \
	php/classes/errors/post-type.php \
	php/classes/errors/taxonomy.php \
	php/classes/errors/wp.php \
	php/classes/interfaces/course.php \
	php/classes/interfaces/episode.php \
	php/classes/interfaces/podcast.php \
	php/classes/interfaces/series.php \
	php/classes/lesson.php \
	php/classes/license.php \
	php/classes/series.php \
	php/classes/traits/course.php \
	php/classes/traits/podcast.php \
	php/includes/autoloader.php \
	php/includes/back-end/actions.php \
	php/includes/back-end/fields.php \
	php/includes/back-end/filters.php \
	php/includes/both-ends/actions.php \
	php/includes/both-ends/api.php \
	php/includes/both-ends/constants.php \
	php/includes/both-ends/filters.php \
	php/includes/both-ends/functions.php \
	php/includes/both-ends/post-types.php \
	php/includes/both-ends/taxonomies.php \
	php/includes/front-end/actions.php \
	php/includes/front-end/filters.php \
	php/includes/plugin.php

potfile = $(langdir)/$(langdomain).pot
version = 0.1


#
# Special targets
#

all:
	:

.PHONY: clean lint maintainer-clean mrproper

.SUFFIXES:

.SUFFIXES: .mo .po


#
# Translation
#

.po.mo:
	$(MSGFMT) -o$@ $<

$(potfile):
	$(WP) i18n make-pot . $(potfile)


#
# Distribution
#

dist: $(distar) $(distar).asc

$(distar): $(distfiles)
	$(PATHCHK) $(PATHCHKFLAGS) -Pp $(distfiles)
	$(TAR) $(TARFLAGS) $(distfiles) | $(GZIP) $(GZIPFLAGS) >$(distar)

$(distar).asc: $(distar)
	$(GPG) $(GPGFLAGS) $(distar)


#
# Plugin
#

plugin: $(pluginar)

$(pluginar): $(pluginfiles)
	$(PATHCHK) $(PATHCHKFLAGS) -Pp $(pluginfiles)
	mkdir -p $(plugin)
	find $(pluginfiles) -exec dirname '{}' ';' | sort -u | sed 's/^/$(plugin)\//' | xargs -E '' mkdir -p
	find $(pluginfiles) -exec cp '{}' '$(plugin)/{}' ';'
	$(ZIP) -r $@ $(plugin)
	rm -rf $(plugin)


#
# Documentation
#

TAGS: tags

tags: $(scripts)
	$(CTAGS) $(CTAGSFLAGS) $(scripts)


#
# Clean-up
#

maintainer-clean: mrproper
	rm -f $(langdir)/*

mrproper: clean
	rm tags

clean:
	rm -f $(distar)


#
# Linters
#

lint:
	$(PHPSTAN) $(PHPSTANFLAGS) analyze . || [ $$? -eq 127 ]
	$(PHPCSFIXER) fix $(PHPCSFIXERFLAGS) . || [ $$? -eq 127 ]
