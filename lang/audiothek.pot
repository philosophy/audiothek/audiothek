# Copyright (C) 2024 Odin Kroeger
# This file is distributed under the same license as the Audiothek plugin.
msgid ""
msgstr ""
"Project-Id-Version: Audiothek 0.1.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/audiothek\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2024-03-14T09:47:29+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.10.0\n"
"X-Domain: audiothek\n"

#. Plugin Name of the plugin
#: audiothek.php
msgid "Audiothek"
msgstr ""

#. Description of the plugin
#: audiothek.php
msgid "<a href='https://wordpress.org/plugins/seriously-simple-podcasting'>Seriously Simple Podcasting</a> extension."
msgstr ""

#. Author of the plugin
#: audiothek.php
msgid "Odin Kroeger"
msgstr ""

#. Author URI of the plugin
#: audiothek.php
msgid "mailto:odin.kroeger@univie.ac.at"
msgstr ""

#: php/classes/errors/date-conv.php:23
msgid "string \"%s\": could not be converted into Unix-time"
msgstr ""

#: php/classes/errors/init.php:25
msgid "Plugin %s: Could not be initialised: %s"
msgstr ""

#: php/classes/errors/post-type.php:23
msgid "post %d: not of post type %s"
msgstr ""

#: php/classes/errors/taxonomy.php:23
msgid "term %d: does not belong to taxonomy %s"
msgstr ""

#: php/includes/back-end/fields.php:37
msgid "Base Classification"
msgstr ""

#: php/includes/back-end/fields.php:75
msgid "People"
msgstr ""

#: php/includes/back-end/fields.php:82
msgid "People you talk about, if any."
msgstr ""

#: php/includes/back-end/fields.php:154
#: php/includes/back-end/fields.php:292
msgid "Episode"
msgstr ""

#: php/includes/back-end/fields.php:161
msgid "No."
msgstr ""

#: php/includes/back-end/fields.php:168
msgid "Leave empty to count automatically."
msgstr ""

#: php/includes/back-end/fields.php:228
msgid "Lesson"
msgstr ""

#: php/includes/back-end/fields.php:235
#: php/includes/back-end/fields.php:422
msgid "Course"
msgstr ""

#: php/includes/back-end/fields.php:299
msgid "Format"
msgstr ""

#: php/includes/back-end/fields.php:306
msgid "\"Philosophical Fragments\" only."
msgstr ""

#: php/includes/back-end/fields.php:359
msgid "Series"
msgstr ""

#: php/includes/back-end/fields.php:363
msgid "Keywords"
msgstr ""

#: php/includes/back-end/fields.php:370
msgid "Use existing keywords if they are close enough."
msgstr ""

#: php/includes/back-end/fields.php:426
msgid "Semester"
msgstr ""

#: php/includes/back-end/fields.php:452
msgid "Course ID"
msgstr ""

#: php/includes/back-end/fields.php:459
msgid "E.g., \"180123\" or \"180123-1\"."
msgstr ""

#: php/includes/back-end/fields.php:479
msgid "Image"
msgstr ""

#: php/includes/back-end/fields.php:530
msgid "Copyright"
msgstr ""

#: php/includes/back-end/fields.php:548
msgid "None"
msgstr ""

#: php/includes/back-end/fields.php:549
msgid "Share work"
msgstr ""

#: php/includes/back-end/fields.php:550
msgid "Derive works"
msgstr ""

#: php/includes/back-end/filters.php:116
msgid "Please use the format ###### or ######-#; e.g., \"180123\" or \"180123-4\"."
msgstr ""

#: php/includes/back-end/filters.php:145
msgid "Please use the format ####W or ####S; e.g., \"2023W\"."
msgstr ""

#: php/includes/back-end/filters.php:158
msgid "%d is too far in the past."
msgstr ""

#: php/includes/back-end/filters.php:168
msgid "%d is too far in the future."
msgstr ""

#: php/includes/both-ends/functions.php:39
msgid "Add new %s"
msgstr ""

#: php/includes/both-ends/functions.php:45
msgid "Add or remove %s"
msgstr ""

#: php/includes/both-ends/functions.php:51
msgid "All %s"
msgstr ""

#: php/includes/both-ends/functions.php:57
msgid "Choose from the most used %s"
msgstr ""

#: php/includes/both-ends/functions.php:63
msgid "Edit %s"
msgstr ""

#: php/includes/both-ends/functions.php:69
msgid "Search %s"
msgstr ""

#: php/includes/both-ends/functions.php:75
msgid "New %s name"
msgstr ""

#: php/includes/both-ends/functions.php:81
msgid "Popular %s"
msgstr ""

#: php/includes/both-ends/functions.php:87
msgid "Separate %s with commas"
msgstr ""

#: php/includes/both-ends/functions.php:93
msgid "Update %s"
msgstr ""

#: php/includes/both-ends/post-types.php:33
msgctxt "taxonomy singular name"
msgid "Lesson"
msgstr ""

#: php/includes/both-ends/post-types.php:38
msgctxt "taxonomy plural name"
msgid "Lessons"
msgstr ""

#: php/includes/both-ends/taxonomies.php:26
msgctxt "taxonomy singular name"
msgid "Course"
msgstr ""

#: php/includes/both-ends/taxonomies.php:31
msgctxt "taxonomy plural name"
msgid "Courses"
msgstr ""

#: php/includes/both-ends/taxonomies.php:49
msgctxt "taxonomy singular name"
msgid "Episode Type"
msgstr ""

#: php/includes/both-ends/taxonomies.php:54
msgctxt "taxonomy plural name"
msgid "Episode Types"
msgstr ""

#: php/includes/both-ends/taxonomies.php:73
msgctxt "taxonomy singular name"
msgid "Migration status"
msgstr ""

#: php/includes/both-ends/taxonomies.php:78
msgctxt "taxonomy plural name"
msgid "Migration statuses"
msgstr ""

#: php/includes/both-ends/taxonomies.php:97
msgctxt "taxonomy singular name"
msgid "Person"
msgstr ""

#: php/includes/both-ends/taxonomies.php:102
msgctxt "taxonomy plural name"
msgid "People"
msgstr ""

#: php/includes/both-ends/taxonomies.php:121
msgctxt "taxonomy singular name"
msgid "Semester"
msgstr ""

#: php/includes/both-ends/taxonomies.php:126
msgctxt "taxonomy plural name"
msgid "Semesters"
msgstr ""
