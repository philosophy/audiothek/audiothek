<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Model for a Creative Commons License.
 */
class License {
    /* Base name for Creative Commons licenses. */
    public const BASE_NAME = 'CC %s v4.0';

    /* Base URL for Creative Commons licenses. */
    public const BASE_URL = 'http://creativecommons.org/licenses/%s/4.0/';

    /* License name. */
    public readonly string $name;

    /* License URL. */
    public readonly string $url;

    /*
     * Create a new license representation.
     * $rights must be "derive" or "share".
     */
    public function __construct(string $rights, bool $commercial)
    {
        $conditions = ['by'];

        if (!$commercial) {
            $conditions[] = 'nc';
        }

        $conditions[] = match ($rights) {
            'derive' => 'sa',
            'share' => 'nd'
        };

        $tag = implode('-', $conditions);
        $this->name = sprintf($this::BASE_NAME, strtoupper($tag));
        $this->url = sprintf($this::BASE_URL, $tag);
    }
}
