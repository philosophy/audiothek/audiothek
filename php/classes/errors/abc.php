<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Errors;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Abstract base class for errors
 */
abstract class ABC extends \Exception
{
    /* Get a printf format string to generate the error message from. */
    abstract protected static function get_format(): string;

    /* Generate a message by substituting $args into $this::get_format(). */
    public function __construct(...$args)
    {
        parent::__construct(sprintf($this::get_format(), ...$args));
    }

    /* Log the error. */
    public function log(): bool
    {
        return error_log($this->getMessage());
    }

    /* Show the error as notification in the WordPress back-end. */
    public function notify(string $level = 'error'): void
    {
        add_action(
            hook_name: 'admin_notices',
            callback: fn () => <<<EOF
                <div class='notice notice-{$level}'>
                    <p>{$this->getMessage()}</p>
                </div>
            EOF
        );
    }
}
