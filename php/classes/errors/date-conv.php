<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Errors;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Date conversion error
 */
class DateConv extends ABC
{
    protected static function get_format(): string
    {
        return esc_html__(
            'string "%s": could not be converted into Unix-time',
            'audiothek'
        );
    }
}
