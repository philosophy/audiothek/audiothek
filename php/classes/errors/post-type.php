<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Errors;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Wrong post type error
 */
class PostType extends ABC
{
    protected static function get_format(): string
    {
        return esc_html__('post %d: not of post type %s', 'audiothek');
    }
}
