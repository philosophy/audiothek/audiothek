<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Errors;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Wrong taxonomy error
 */
class Taxonomy extends ABC
{
    protected static function get_format(): string
    {
        return esc_html__(
            'term %d: does not belong to taxonomy %s',
            'audiothek'
        );
    }
}
