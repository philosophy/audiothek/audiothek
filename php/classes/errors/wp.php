<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Errors;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Wrapper for WordPress errors
 */
class WP extends ABC
{
    protected static function get_format(): string
    {
        return '%s';
    }

    public function __construct(public readonly \WP_Error $error)
    {
        parent::__construct(rtrim($error->get_error_message(), '.'));
    }
}
