<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Errors;

use const Univie\Audiothek\PLUGIN_FILE;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Wrapper for initialisation errors
 */
class Init extends ABC
{
    protected static function get_format(): string
    {
        return esc_html__(
            'Plugin %s: Could not be initialised: %s',
            'audiothek'
        );
    }

    public function __construct(public readonly \Exception $error)
    {
        parent::__construct(PLUGIN_FILE, $error->getMessage());
    }
}
