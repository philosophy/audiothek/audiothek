<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Model for episodes.
 */
class Episode implements Interfaces\Episode
{
    use Traits\Podcast;

    /* Series class that corresponds to this episode class. */
    public const SERIES_CLASS = __NAMESPACE__ . '\\Series';

    /* Post type that corresponds to this episode class. */
    public const POST_TYPE = SSP_CPT_PODCAST;

    /* Create an episode interface to $post. */
    public function __construct(protected readonly \WP_Post $post)
    {
        if ($post->post_type !== $this::POST_TYPE) {
            throw new Errors\PostType($post->ID, $this::POST_TYPE);
        }
    }

    public function get_image_id(): ?int
    {
        $attachment_id = $this->get_meta('cover_image_id', true);

        if ($attachment_id !== '') {
            return (int) $attachment_id;
        }

        $attachment_id = $this->from_series(__FUNCTION__);

        if ($attachment_id !== '') {
            return (int) $attachment_id;
        }

        return null;
    }

    public function get_license(): ?License
    {
        $inherit = $this->get_meta(FLD_INHERIT_LICENSE, true);
        if ($inherit === '' || $inherit === true) {
	    $obj = $this->get_series_iterator()->current();
	    if ($obj) {
                return $obj->get_license();
	    }
        }

        $rights = $this->get_meta(FLD_RIGHTS, true);
        if ($rights === '') {
            return null;
        }

        $commercial = $this->get_meta(FLD_COMMERCIAL_USE, true);

        return new License($rights, (bool) $commercial);
    }

    public function get_number(): ?int
    {
        $number = get_metadata('post', $this->post->ID, FLD_NUMBER, true);
        if ($number === null || $number === '') {
            return null;
        }

        return (int) $number;
    }

    public function get_recording_date(): ?int
    {
        $date = $this->get_meta('date_recorded', true);
        if ($date === '') {
            return null;
        }

        $secs = strtotime($date);
        if ($secs === false) {
            throw new Errors\DateConv($date);
        }

        return $secs;
    }

    public function get_series(bool $single = true): array|\WP_Term|null
    {
        $taxonomy = $this::SERIES_CLASS::TAXONOMY;

        $terms = $this->get_post_terms($taxonomy);

        if (count($terms) === 0) {
            return $single ? null : [];
        }

        if ($single) {
            $terms = array_splice($terms, 0, 1);
        }

        $translations = terms_translate($terms, $taxonomy);

        return $single ? $translations[0] : $translations;
    }

    public function get_terms(string $taxonomy): array
    {
        $terms = $this->get_post_terms($taxonomy);

        if (count($terms) === 0) {
            $terms = $this->from_series(__FUNCTION__, $taxonomy);

            if ($terms === null) {
                return [];
            }
        }

        return $terms;
    }

    /* Call $callable(...$args) via the episode's series. */
    protected function from_series(string $callable, ...$args)
    {
        foreach ($this->get_series_iterator() as $obj) {
            $value = $obj->$callable(...$args);
            if ($value !== null) {
                return $value;
            }
        }

        return null;
    }

    /* Get metadata associated with the episode. */
    protected function get_meta(string $key, bool $single): mixed
    {
        return get_metadata('post', $this->post->ID, $key, $single);
    }

    /* Get the terms associated with the post for the given taxonomy. */
    protected function get_post_terms(string $taxonomy): array
    {
        $terms = get_the_terms($this->post, $taxonomy);

        if ($terms === false) {
            return [];
        }

        if (is_wp_error($terms)) {
            throw new Errors\WP($terms);
        }

        return $terms;
    }

    /* Generate an interface for each series the episode belongs to. */
    protected function get_series_iterator(): \Iterator
    {
        $class = $this::SERIES_CLASS;

        foreach ($this->get_post_terms($class::TAXONOMY) as $term) {
            yield new $class($term);
        }
    }
}
