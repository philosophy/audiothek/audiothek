<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Traits;

use Univie\Audiothek;
use Univie\Audiothek\Interfaces;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Partial implementation of the podcast interface.
 */
trait Podcast
{
    abstract public function get_terms(string $taxonomy): array;

    abstract protected function get_meta(string $key, bool $single): mixed;

    abstract protected function get_series_iterator(): \Iterator;

    public function is_course(): bool
    {
        return $this instanceof Interfaces\Course;
    }

    public function get_classifications(): array
    {
        $terms = $this->get_terms(Audiothek\TAX_CLASSIFICATION);
        Audiothek\terms_sort($terms);
        return $terms;
    }

    public function get_license(): ?Audiothek\License
    {
        $inherit = $this->get_meta(Audiothek\FLD_INHERIT_LICENSE, true);
        if ($inherit === '' || $inherit === true) {
            $obj = $this->get_series_iterator()->current();
            if ($obj === null) {
                return null;
            }

            return $obj->get_license();
        }

        $rights = $this->get_meta(Audiothek\FLD_RIGHTS, true);
        if ($rights === '' || $rights === 'nil') {
            return null;
        }

        $commercial = $this->get_meta(Audiothek\FLD_COMMERCIAL_USE, true);

        return new Audiothek\License($rights, (bool) $commercial);
    }

    public function get_persons(): array
    {
        $terms = $this->get_terms(Audiothek\TAX_PERSONS);
        Audiothek\terms_sort($terms);
        return $terms;
    }

    public function get_speakers(): array
    {
        return $this->get_terms(Audiothek\TAX_SPEAKER);
    }

    /* FIXME: Unused. */
    public function get_tags(): array
    {
        $terms = $this->get_terms(Audiothek\TAX_TAGS);
        Audiothek\terms_sort($terms);
        return $terms;
    }
}
