<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Traits;

use Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Partial implementation of the course interface.
 */
trait Course
{
    abstract public function get_course_id(): ?string;

    public function get_course_url(): ?string
    {
        $semester = $this->get_semester();
        if (!$semester) {
            return null;
        }

        $course_id = $this->get_course_id();
        if (!$course_id) {
            return null;
        }

        return sprintf(
            'https://ufind.univie.ac.at/%s/course.html?lv=%s&semester=%s',
            Audiothek\LANG,
            $course_id,
            $semester->name
        );
    }

    public function get_semester(): ?\WP_Term
    {
        $semesters = $this->get_terms(Audiothek\TAX_SEMESTER);
        return count($semesters) === 0 ? null : $semesters[0];
    }
}
