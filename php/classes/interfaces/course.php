<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Interfaces;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Course interface
 */
interface Course extends Podcast
{
    /* Get the course ID ("LV-Nummer"), if set. */
    public function get_course_id(): ?string;

    /* Get the course's u:find URL, if the course ID and semester are set. */
    public function get_course_url(): ?string;

    /* Get the semester the course was held in, if set. */
    public function get_semester(): ?\WP_Term;
}
