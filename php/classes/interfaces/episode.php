<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Interfaces;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Episode interface
 */
interface Episode extends Podcast
{
    /* Get the episode sequence number, if set. */
    public function get_number(): ?int;

    /* Get the recording date as seconds since the epoch, if set. */
    public function get_recording_date(): ?int;

    /* Get the series the episode belongs to, if any. */
    public function get_series(bool $single = true): array|\WP_Term|null;
}
