<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Interfaces;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Series interface
 */
interface Series extends Podcast
{
    /* Get the URL of the podcast feed. */
    public function get_feed_url(): string;

    /* Check whether episodes have been published in this series. */
    public function has_episodes(): bool;
}
