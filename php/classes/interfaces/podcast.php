<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Interfaces;

use Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Podcast interface
 */
interface Podcast
{
    /* Get the terms associated with the podcast for the given $taxonomy. */
    public function get_terms(string $taxonomy): array;

    /* Check whether the episode belongs to/the series is a course. */
    public function is_course(): bool;

    /* Get the Basisklassifikation classes of a series or episode. */
    public function get_classifications(): array;

    /* Get the ID of the album image, if set. */
    public function get_image_id(): ?int;

    /* Get the license for third parties, if one was granted. */
    public function get_license(): ?Audiothek\License;

    /* Get the people spoken about. */
    public function get_persons(): array;

    /* Get the speakers of an episode or a series. */
    public function get_speakers(): array;

    /* Get the tags. */
    public function get_tags(): array;
}
