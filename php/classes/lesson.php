<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Model for individual lessons (as opposed to courses as a whole).
 */
class Lesson extends Episode implements Interfaces\Episode, Interfaces\Course
{
    use Traits\Course;

    public const SERIES_CLASS = __NAMESPACE__ . '\\Course';

    public const POST_TYPE = CPT_LESSON;

    public function get_course_id(): ?string
    {
        return $this->from_series(__FUNCTION__);
    }

    public function get_image_id(): ?int
    {
        $attachment_id = $this->post->cover_image_id;
        if ($attachment_id !== '') {
            return (int) $attachment_id;
        }

        $attachment_id = $this->from_series(__FUNCTION__);
        if ($attachment_id !== null) {
            return (int) $attachment_id;
        }

        return null;
    }

    public function get_semester(): ?\WP_Term
    {
        return $this->from_series(__FUNCTION__);
    }
}
