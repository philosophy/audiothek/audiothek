<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Model for series.
 */
class Series implements Interfaces\Series
{
    use Traits\Podcast;

    static protected function get_parent(int $term_id): int|false
    {
        return wp_get_term_taxonomy_parent_id($term_id, self::TAXONOMY);
    }

    /* The taxonomy corresponding to this class. */
    public const TAXONOMY = TAX_SERIES;

    /* Create a series interface to $term. */
    public function __construct(protected readonly \WP_Term $term)
    {
        if ($term->taxonomy !== $this::TAXONOMY) {
            throw new Errors\Taxonomy($term->term_id, $this::TAXONOMY);
        }
    }

    public function get_terms(string $taxonomy): array
    {
        $values = $this->get_meta(PREFIX . $taxonomy, false);

        if (count($values) === 0) {
            return [];
        }

        /* ACF nests arrays. Still, this is ugly. */
        if (count($values) === 1 && is_array($values[0])) {
            $values = $values[0];
        }

        return array_filter(
            array_map(
                fn ($term_id) => get_term($term_id, $taxonomy),
                $values
            ),
            fn ($res) => $res !== null && !is_wp_error($res)
        );
    }

    public function get_feed_url(): string
    {
        $feed_url = ssp_get_feed_url($this->term->slug);
        return preg_replace('~^https?:~', 'podcast:', $feed_url, 1);
    }

    public function get_image_id(): ?int
    {
        $attachment_id = $this->get_meta(
            SSP_CPT_PODCAST . '_series_image_settings',
            true
        );

        if ($attachment_id !== '') {
            return (int) $attachment_id;
        }

        return null;
    }

    public function has_episodes(): bool
    {
        return $this->term->count > 0;
    }

    /* Get the value of a metadata field. */
    protected function get_meta(string $key, bool $single): mixed
    {
        return get_metadata('term', $this->term->term_id, $key, $single);
    }

    /* Get instances of the series that the series is a child of. */
    protected function get_series_iterator(): \Iterator
    {
        for (
            $term_id = $this::get_parent($this->term->term_id);
            $term_id;
            $term_id = $this::get_parent($term_id)
        ) {
            $term = get_term_by('term_taxonomy_id', $term_id);

            if (!$term) {
                /* FIXME: Raise an error. */
            }

            yield new self($term);
        }
    }
}
