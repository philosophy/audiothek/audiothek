<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Model for courses as a whole (as opposed to individual lessons).
 */
class Course extends Series implements Interfaces\Series, Interfaces\Course
{
    use Traits\Course;

    public const TAXONOMY = TAX_COURSE;

    public function get_course_id(): ?string
    {
        $course_id = get_metadata(
            'term',
            $this->term->term_id,
            FLD_COURSE_ID,
            true
        );

        if ($course_id === '') {
            return null;
        }

        return $course_id;
    }

    public function get_feed_url(): string
    {
        $feed_url = sprintf(
            '%s/%s/%s/feed',
            get_site_url(),
            $this::TAXONOMY,
            $this->term->slug
        );

        return preg_replace('/^https?:/', 'podcast:', $feed_url, 1);
    }

    public function get_image_id(): ?int
    {
        $attachment_id = get_metadata(
            'term',
            $this->term->term_id,
            FLD_IMAGE_ID,
            true
        );

        if ($attachment_id !== '') {
            return (int) $attachment_id;
        }

        return null;
    }
}
