<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Prefix
 */

const PREFIX = 'audiothek_';


/*
 * Site language
 */

define(
    __NAMESPACE__ . '\\LANG',
    substr(
        function_exists('pll_current_language') ?
            /* pll_current_language may return false */
            pll_current_language() ?: get_locale() :
            /* pll_current_language does not exist */
            get_locale(),
        0,
        2
    )
);


/*
 * Models
 */

/* List of classes that implement the series interface. */
const CLS_SERIES = [
    __NAMESPACE__ . '\\Series',
    __NAMESPACE__ . '\\Course'
];

/* List of classes that implement the episode interface. */
const CLS_EPISODE = [
    __NAMESPACE__ . '\\Episode',
    __NAMESPACE__ . '\\Lesson'
];


/*
 * Post type names
 */

/* Name of the post type for lessons. */
const CPT_LESSON = 'lesson';


/*
 * Taxonomy names
 */

/* Name of Basisklassifikation taxonomy (provided by Basisklassifikation). */
const TAX_CLASSIFICATION = \Univie\Basisklassifikation\TAXONOMY;

/* Name of course taxonomy. */
const TAX_COURSE = 'course';

// NO LONGER IN USE FIXME
///* Name the Philosophical Fragments episode type taxonomy. */
//const TAX_FORMAT = 'format';

/* Name of the taxonomy for people being spoken about. */
const TAX_PERSONS = 'persons';

/* Name of semester taxonomy. */
const TAX_SEMESTER = 'semester';

/* Taxonomy for editing notes. */
const TAX_MIGRATION = 'transition';

/* Name of speaker taxonomy (provided by Seriously Simple Speakers). */
const TAX_SPEAKER = 'speaker';

/* Name of the tags taxonomy (provided by WordPress core). */
const TAX_TAGS = 'tags';

/* Name of series taxonomy (provided by SSP). */
define('TAX_SERIES', ssp_series_slug());


/*
 * Custom field names
 */

/*
 * The name of every field MUST start with PREFIX. Moreover, if a field
 * references a taxonomy term, the remainder of the field's name SHOULD
 * be the name of that taxonomy; for example, "audiothek_course".
 */

/* Name of the metadata field for Basisklassifikation classes. */
const FLD_CLASSIFICATION = PREFIX . TAX_CLASSIFICATION;

/* Name of the metadata field for allow commercial use. */
const FLD_COMMERCIAL_USE = PREFIX . 'allow_commercial_use';

/* Name of course metadata field. */
const FLD_COURSE = PREFIX . TAX_COURSE;

/* Name of course ID ("LV-Nummer") metadata field. */
const FLD_COURSE_ID = PREFIX . 'course_id';

///* Name of the metadata field for the episode format. */
//const FLD_FORMAT = PREFIX . TAX_FORMAT;

/* Name of the metadata field for license inheritance. */
const FLD_INHERIT_LICENSE = PREFIX . 'inherit_license';

/* Name of the metadata field for album art image IDs. */
const FLD_IMAGE_ID = PREFIX . 'image_id';

/* Name of keyword metadata field. */
const FLD_TAGS = PREFIX . TAX_TAGS;

/* Name of episode number metadata field. */
const FLD_NUMBER = PREFIX . 'number';

/* Name of the metadata field for people being spoken about. */
const FLD_PERSONS = PREFIX . TAX_PERSONS;

/* Name of the metadata field for granted rights. */
const FLD_RIGHTS = PREFIX . 'rights_granted';

/* Name of semester metadata field. */
const FLD_SEMESTER = PREFIX . TAX_SEMESTER;

/* Name of speakers metadata field. */
const FLD_SPEAKERS = PREFIX . TAX_SPEAKER;

