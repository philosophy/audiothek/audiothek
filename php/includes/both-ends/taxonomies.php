<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Taxonomies
 */

/* Courses. */
register_taxonomy(
    taxonomy: TAX_COURSE,
    object_type: CPT_LESSON,
    args: [
        'labels' => labels_derive(
            singular: esc_html_x(
                'Course',
                'taxonomy singular name',
                'audiothek'
            ),
            plural: esc_html_x(
                'Courses',
                'taxonomy plural name',
                'audiothek'
            ),
        ),
        'meta_box_cb' => false,
        'show_in_rest' => false,
    ]
);


/* 
 * TODO: Once everything has been corrected; create a new taxnomy
 *       for editing statuses with a proper taxonomy name.
 */
/* Special taxonomy for data migration. */
register_taxonomy(
    taxonomy: TAX_MIGRATION,
    object_type: [CPT_LESSON, SSP_CPT_PODCAST],
    args: [
        'labels' => labels_derive(
            singular: esc_html_x(
                'Editing remark',
                'taxonomy singular name',
                'audiothek'
            ),
            plural: esc_html_x(
                'Editing remarks',
                'taxonomy plural name',
                'audiothek'
            ),
        ),
        'meta_box_cb' => false,
        'show_in_rest' => true,
        'show_admin_column' => true,
    ]
);

/* People talked about. */
register_taxonomy(
    taxonomy: TAX_PERSONS,
    object_type: [CPT_LESSON, SSP_CPT_PODCAST],
    args: [
        'labels' => labels_derive(
            singular: esc_html_x(
                'Person',
                'taxonomy singular name',
                'audiothek'
            ),
            plural: esc_html_x(
                'People',
                'taxonomy plural name',
                'audiothek'
            ),
        ),
        'meta_box_cb' => false,
        'show_in_rest' => false,
        'show_admin_column' => true,
    ]
);


/* Semesters. */
register_taxonomy(
    taxonomy: TAX_SEMESTER,
    object_type: CPT_LESSON,
    args: [
        'labels' => labels_derive(
            singular: esc_html(_x(
                'Semester',
                'taxonomy singular name',
                'audiothek'
            )),
            plural: esc_html(_x(
                'Semesters',
                'taxonomy plural name',
                'audiothek'
            )),
        ),
        'meta_box_cb' => false,
        'show_in_rest' => false,
    ]
);
