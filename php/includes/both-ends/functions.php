<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Functions
 */

/* Derive a WordPress labels array from basic forms. */
function labels_derive(
    string $singular,
    string $plural,
    ?string $name = null,
    ?string $menu = null
): array {
    return [
        'name' =>
            $name ?? $plural,

        'singular_name' =>
            $singular,

        'menu_name' =>
            $menu ?? $name ?? $plural,

        'add_new_item' =>
            sprintf(
                esc_html__('Add new %s', 'audiothek'),
                $singular
            ),

        'add_or_remove_items' =>
            sprintf(
                esc_html__('Add or remove %s', 'audiothek'),
                $plural
            ),

        'all_items' =>
            sprintf(
                esc_html__('All %s', 'audiothek'),
                $plural
            ),

        'choose_from_most_used' =>
            sprintf(
                esc_html__('Choose from the most used %s', 'audiothek'),
                $plural
            ),

        'edit_item' =>
            sprintf(
                esc_html__('Edit %s', 'audiothek'),
                $singular
            ),

        'search_items' =>
            sprintf(
                esc_html__('Search %s', 'audiothek'),
                $singular
            ),

        'new_item_name' =>
            sprintf(
                esc_html__('New %s name', 'audiothek'),
                $singular
            ),

        'popular_items' =>
            sprintf(
                esc_html__('Popular %s', 'audiothek'),
                $plural
            ),

        'separate_items_with_commas' =>
            sprintf(
                esc_html__('Separate %s with commas', 'audiothek'),
                $plural
            ),

        'update_item' =>
            sprintf(
                esc_html__('Update %s', 'audiothek'),
                $singular
            ),
    ];
}

/* Sort $terms in-place by name. */
function terms_sort(array &$terms): void
{
    uasort(
        $terms,
        fn (\WP_Term $a, \WP_Term $b) => strnatcmp($a->name, $b->name)
    );
}

/* Translate $terms from $taxonomy if Polylang is available. */
if (
    function_exists('pll_get_term')
    && function_exists('pll_is_translated_taxonomy')
) {
    function terms_translate(array $terms, string $taxonomy): array
    {
        if (!pll_is_translated_taxonomy($taxonomy)) {
            return $terms;
        }

        return array_map(
            function (\WP_Term $term) use ($taxonomy): \WP_Term {
                $id = pll_get_term($term->term_taxonomy_id);
                if ($id === 0) {
                    return $term;
                }

                $translation = get_term_by('term_id', $id, $taxonomy);
                if ($translation === false) {
                    return $term;
                }

                return $translation;
            },
            $terms
        );
    }
} else {
    function terms_translate(array $terms, string $taxonomy): array
    {
        return $terms;
    }
}
