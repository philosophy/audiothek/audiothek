<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Post types
 */

/* Course episodes. */
register_post_type(
    post_type: CPT_LESSON,
    args: [
        'taxonomies' => [
            TAX_COURSE,
            TAX_CLASSIFICATION,
	        TAX_MIGRATION,
            TAX_SPEAKER,
            TAX_TAGS,
            TAX_SEMESTER
        ],
        'labels' => labels_derive(
            singular: esc_html_x(
                'Lesson',
                'taxonomy singular name',
                'audiothek'
            ),
            plural: esc_html_x(
                'Lessons',
                'taxonomy plural name',
                'audiothek'
            )
        ),
        'public' => true,
        'publicly_queryable' => true,
        'menu_icon' => 'dashicons-book',
        'menu_position' => 4,
        'show_in_rest' => true,
        'capability_type' => SSP_CPT_PODCAST,
        'supports' => [
            'title',
            'editor',
            'revisions',
            'excerpt',
            'thumbnail',
            'custom-fields'
        ],
        'has_archive' => true
    ]
);
