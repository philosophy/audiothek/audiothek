<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Actions
 */

/* Unregister series taxonomy for lessons. */
if (function_exists('ssp_series_taxonomy')
    && function_exists('ssp_post_types'))
{
    function no_series_for_lessons(
        string $taxonomy,
        array|string $object_type,
        array $args
    ) {
        if ($taxonomy == ssp_series_taxonomy()) {
            $posttypes = array_filter(
                ssp_post_types(),
                function(string $var): bool { return $var != CPT_LESSON; }
            );

            if ($posttypes) {
                remove_action(
                    'registered_taxonomy',
                    '\Univie\Audiothek\no_series_for_lessons'
                );

                register_taxonomy(
                    $taxonomy,
                    array_filter(
                        $posttypes,
                        function(string $var): bool {
                            return $var != CPT_LESSON;
                        }
                    ),
                    $args
                );
            }
        }
    }

    add_action(
        hook_name: 'registered_taxonomy',
        callback: '\Univie\Audiothek\no_series_for_lessons',
        priority: 10,
        accepted_args: 3
    );
}
