<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Functions
 */

/*
 * Get an episode interface for the $post, iff $post is an episode.
 * $post defaults to the current post.
 */
function get_audiothek_episode(
    ?WP_Post $post = null
): ?\Univie\Audiothek\Interfaces\Episode {
    if ($post === null) {
        $post = $GLOBALS['post'];

        if ($post === null) {
            return null;
        }
    }

    foreach (\Univie\Audiothek\CLS_EPISODE as $class) {
        try {
            return new $class($post);
        } catch (\Univie\Audiothek\Errors\PostType) {
            /* Empty on purpose. */
        } catch (\Exception $e) {
            if (defined('\\WP_DEBUG') && WP_DEBUG) {
                throw $e;
            }

            return null;
        }
    }

    return null;
}

/*
 * Get a series interface for $term, iff $term is a series.
 * $term defaults to the currently queried object.
 */
function get_audiothek_series(
    ?WP_Term $term = null
): ?\Univie\Audiothek\Interfaces\Series {
    if ($term === null) {
        $term = get_queried_object();

        if ($term === null || !$term instanceof WP_Term) {
            return null;
        }
    }

    foreach (\Univie\Audiothek\CLS_SERIES as $class) {
        try {
            return new $class($term);
        } catch (\Univie\Audiothek\Errors\Taxonomy) {
            /* Empty on purpose. */
        } catch (\Exception $e) {
            if (defined('\\WP_DEBUG') && WP_DEBUG) {
                throw $e;
            }

            return null;
        }
    }

    return null;
}
