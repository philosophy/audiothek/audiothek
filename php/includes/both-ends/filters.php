<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Filters
 */

/* Register CPT_LESSON as a podcast post type. */
add_filter(
    hook_name: 'ssp_podcast_post_types',
    callback: function (array $types): array {
        $types[] = CPT_LESSON;
        return $types;
    }
);

/* Add the Basisklassifikation to podcast post types. */
add_filter(
    hook_name: 'basisklassifikation/post_types',
    callback: function (array $types): array {
        return array_merge($types, [CPT_LESSON, SSP_CPT_PODCAST]);
    }
);
