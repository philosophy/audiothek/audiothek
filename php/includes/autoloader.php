<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Autoloader;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Functions
 */

/* Convert from CamelCase to kebab-case. */
function _kebabify(string $camel): ?string
{
    return strtolower(preg_replace('/(\w)([A-Z][a-z])/', '$1-$2', $camel));
}

/* Generate a function that loads classes in $namespace from $basedir. */
function _make(string $namespace, string $basedir): callable
{
    assert(path_is_absolute($basedir));
    assert(is_dir($basedir));

    $offset = strlen($namespace);
    $basedir = rtrim($basedir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

    return function (string $class) use ($namespace, $offset, $basedir): bool {
        if (!str_starts_with($class, $namespace)) {
            return false;
        }

        $kebab = _kebabify(ltrim(substr($class, $offset), '\\'));
        if ($kebab === null) {
            wp_die("Could not infer filename of <code>{$class}</code>.");
        }

        $fname = str_replace('\\', DIRECTORY_SEPARATOR, $kebab) . '.php';

        require $basedir . $fname;

        return true;
    };
}

/* Register an auto-loader that loads classes in $namespace from $basedir. */
function register(string $namespace, string $basedir): void
{
    $loader = _make(namespace: $namespace, basedir: $basedir);
    if (spl_autoload_register($loader) === false) {
        wp_die('Could not register auto-loader.');
    }
}
