<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Filters
 */

/* Add episode images to lessons. */
add_filter(
    hook_name: 'ssp_album_art',
    callback: function (
        array $art,
        int $episode_id,
        string $size,
        string $context
    ): array {
        $post = get_post($episode_id);
        if ($post !== null) {
            try {
                $lesson = new Lesson($post);
            } catch (Errors\PostType $e) {
                return $art;
            }

            $image_id = $lesson->get_image_id();
            if ($image_id !== null) {
                $image_src = wp_get_attachment_image_url($image_id);
                if ($image_src !== false) {
                    $art['src'] = $image_src;
                }
            }
        }

        return $art;
    },
    accepted_args: 4
);
