<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Actions
 */

/* Add CPT_LESSON to the main loop. */
add_action(
    hook_name: 'pre_get_posts',
    callback: function (\WP_Query $query): \WP_Query {
        if (!is_admin() && $query->is_main_query() && $query->is_home()) {
            $post_types = $query->get('post_type');
            if (is_array($post_types)) {
                $post_types[] = CPT_LESSON;
            } else {
                $post_types = [$post_types, CPT_LESSON];
            }
            $query->set('post_type', $post_types);
        }
        return $query;
    }
);

/*
 * FIXME: This is temporary; it sorts all posts after all episodes. 
 */

/* Sort posts by recording date. */
add_action(
    hook_name: 'pre_get_posts',
    callback: function (\WP_Query $query): \WP_Query {
        if (!is_admin() && $query->is_main_query() && $query->is_home()) {
                $query->set('meta_query', [
                [
                    'relation' => 'OR',
                    'without_date_recorded' => [
                        'key' => 'date_recorded',
                        'compare' => 'NOT EXISTS'
                    ],
                    'with_date_recorded' => [
                        'key' => 'date_recorded',
                        'compare' => 'EXISTS'
                    ]
                ],
                (array) $query->get('meta_query')
            ]);
            $query->set('orderby', 'without_date_recorded date');
        }
        return $query;
    }
);


