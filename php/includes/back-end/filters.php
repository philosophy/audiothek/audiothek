<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Filters
 */

/* Modify Seriously Simple Podcasting taxonomies. */
add_filter(
    hook_name: 'ssp_register_taxonomy_args',
    callback: function (array $args, string $taxonomy): array {
        switch ($taxonomy) {
            case TAX_SPEAKER:
                $args['sort'] = true;
                /* Falls through. */
            case TAX_SERIES:
                /* Must be complemented by JavaScript. */
                /* It must? */
                $args['show_in_rest'] = true;
                break;
        }
        return $args;
    },
    accepted_args: 2
);


/* Translate Basisklassifikation terms to match the language their post. */
if (defined('FLD_CLASSIFICATION')) {
    if (function_exists('pll_get_term')) {
        add_filter(
            hook_name: 'acf/update_value/name=' . FLD_CLASSIFICATION,
            callback: function (
                mixed $value,
                int|string $post_id,
                array $field,
                mixed $original
            ): mixed {
                if ($_POST !== null) {
                    if ($value && isset($_POST['term_lang_choice'])) {
                        $lang = $_POST['term_lang_choice'];
                        if (!is_wp_error($lang)) {
                            foreach ($value as $i => $term_id) {
                                $translation_id = pll_get_term($term_id, $lang);

                                if (
                                    $translation_id !== 0
                                    && $translation_id != $term_id
                                ) {
                                    $value[$i] = $translation_id;
                                }
                            }
                        }
                    }

                    return $value;
                }
            },
            accepted_args: 4
        );
    }
}

/* Count episodes automatically. */
add_filter(
    hook_name: 'acf/update_value/name=' . FLD_NUMBER,
    callback: function (
        mixed $value,
        int|string $post_id,
        array $field,
        mixed $original
    ): mixed {
        if (!$value) {
            $post = get_post((int) $post_id);
            if ($post !== null) {
                $episode = \get_audiothek_episode($post);
                if ($episode !== null) {
                    $series = $episode->get_series();
                    if ($series !== null) {
                        return $series->count + 1;
                    }
                }
            }
        }

        return $value;
    },
    accepted_args: 4
);

/* Validate course IDs. */
add_filter(
    hook_name: 'acf/validate_value/name=' . FLD_COURSE_ID,
    callback: function (
        bool|string $valid,
        mixed $value,
        array $field,
        string $input_name
    ): bool|string {
        if ($valid && is_string($value)) {
            if (!preg_match('/^\d{6}(?:-\d+|)$/', $value)) {
                return esc_html__(
                    'Please use the format ###### or ######-#; e.g., "180123" or "180123-4".',
                    'audiothek'
                );
            }
        }

        return $valid;
    },
    accepted_args: 4
);

/* Validate semester terms before insertion. */
add_filter(
    hook_name: 'wp_update_term',
    callback: function (
        string|\WP_Error $term,
        string $taxonomy,
        array|string $args
    ): string|\WP_Error {
        if ($taxonomy !== TAX_SEMESTER) {
            return $term;
        }

        $matches = [];

        if (!preg_match('/^(\d{4})[SW]$/', $term, $matches)) {
            return new \WP_Error(
                'audiothek_semester_syntax',
                esc_html__(
                    'Please use the format ####W or ####S; e.g., "2023W".',
                    'audiothek'
                )
            );
        }

        $year = (int) $matches[1];

        if ($year < 1859) {
            return new \WP_Error(
                'audiothek_semester_past',
                sprintf(
                    esc_html__('%d is too far in the past.', 'audiothek'),
                    $year
                )
            );
        }

        if ($year > date('Y') + 1) {
            return new \WP_Error(
                'audiothek_semester_future',
                sprintf(
                    esc_html__('%d is too far in the future.', 'audiothek'),
                    $year
                )
            );
        }

        return strtoupper($term);
    },
    accepted_args: 3
);

