<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Custom fields
 */

/* FIXME: This whole file is a bit of a mess. */
/* FIXME: Order fields by importance. */

/*
 * Metadata fields to register with ACF.
 * Keys and metadata field names MUST be the same.
 * And they MUST start with FIELD_NAME_PREFIX.
 */

if (function_exists('acf_add_local_field_group')) {
    /* Metadata common to all podcasts. */
    acf_add_local_field_group([
        'key' => PREFIX . 'field_group_metadata',
        'title' => 'Metadata',
        'fields' => [
            [
                'key' => FLD_CLASSIFICATION,
                'label' => esc_html__(
                    'Base Classification',
                    'audiothek'
                ),
                'name' => FLD_CLASSIFICATION,
                'aria-label' => '',
                'type' => 'taxonomy',
                'instructions' => esc_html__(<<<EOF
                    Only use "general" classes if the podcast is about
                    that subject as a whole.

                    Save for "Philosophy and theory of …", classes outside of
                    "08 Philosophy" and "02 Science and culture" are for other
                    disciplines.

                    Genera are added automatically.
                    EOF,
                    'audiothek'
                ),
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'relevanssi_exclude' => 0,
                'taxonomy' => TAX_CLASSIFICATION,
                'add_term' => 0,
                'save_terms' => 1,
                'load_terms' => 1,
                'return_format' => 'id',
                'field_type' => 'multi_select',
                'allow_null' => 0,
                'multiple' => 0
            ],
            [
                'key' => FLD_PERSONS,
                'label' => esc_html__(
                    'People',
                    'audiothek'
                ),
                'name' => FLD_PERSONS,
                'aria-label' => '',
                'type' => 'taxonomy',
                'instructions' => esc_html__(
                    'People you talk about, if any.',
                    'audiothek'
                ),
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'relevanssi_exclude' => 0,
                'taxonomy' => TAX_PERSONS,
                'add_term' => 1,
                'save_terms' => 1,
                'load_terms' => 1,
                'return_format' => 'id',
                'field_type' => 'multi_select',
                'allow_null' => 0,
                'multiple' => 0,
            ]
        ],
        'location' => [
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => SSP_CPT_PODCAST,
                ]
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => CPT_LESSON,
                ]
            ],
            [
                [
                    'param' => 'taxonomy',
                    'operator' => '==',
                    'value' => ssp_series_slug(),
                ],
            ],
            [
                [
                    'param' => 'taxonomy',
                    'operator' => '==',
                    'value' => TAX_COURSE,
                ]
            ]
        ],
        'menu_order' => 100,
        'position' => 'side',
        'style' => 'seamless',
        'label_placement' => 'left',
        'instruction_placement' => 'field',
        'hide_on_screen' => [
            'discussion',
            'comments',
            'format',
            'page_attributes',
            'categories',
            'send-trackbacks',
        ],
        'active' => true,
        'show_in_rest' => 0
    ]);

    /* Episode metadata. */
    acf_add_local_field_group([
        'key' => 'audiothek_field_group_episode',
        'title' => esc_html__(
            'Episode',
            'audiothek'
        ),
        'fields' => [
            [
                'key' => FLD_NUMBER,
                'label' => esc_html__(
                    'No.',
                    'audiothek'
                ),
                'name' => FLD_NUMBER,
                'aria-label' => '',
                'type' => 'number',
                'instructions' => esc_html__(
                    'Leave empty to count automatically.',
                    'audiothek'
                ),
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'relevanssi_exclude' => 0,
                'default_value' => '',
                'min' => 1,
                'max' => '',
                'placeholder' => '',
                'step' => '',
                'prepend' => '',
                'append' => '',
            ],
        ],
        'location' => [
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => SSP_CPT_PODCAST,
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => CPT_LESSON,
                ],
            ],
        ],
        'menu_order' => 50,
        'position' => 'side',
        'style' => 'seamless',
        'label_placement' => 'left',
        'instruction_placement' => 'field',
        'hide_on_screen' => [
            'discussion',
            'comments',
            'format',
            'page_attributes',
            'categories',
            'send-trackbacks',
        ],
        'active' => true,
        'description' => '',
        'show_in_rest' => 0
    ]);


    /* Lesson metadata. */
    acf_add_local_field_group(
        [
            'key' => 'audiothek_field_group_lesson',
            'title' => esc_html__(
                'Lesson',
                'audiothek'
            ),
            'fields' => [
                [
                    'key' => FLD_COURSE,
                    'label' => esc_html__(
                        'Course',
                        'audiothek'
                    ),
                    'name' => FLD_COURSE,
                    'aria-label' => '',
                    'type' => 'taxonomy',
                    'required' => true,
                    'conditional_logic' => 0,
                    'wrapper' => [
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ],
                    'relevanssi_exclude' => 1,
                    'taxonomy' => TAX_COURSE,
                    'add_term' => 0,
                    'save_terms' => 1,
                    'load_terms' => 1,
                    'return_format' => 'id',
                    'field_type' => 'select',
                    'allow_null' => 0,
                    'multiple' => 0,
                ],
            ],
            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => CPT_LESSON,
                    ],
                ]
            ],
            'menu_order' => 0,
            'position' => 'side',
            'style' => 'seamless',
            'label_placement' => 'left',
            'instruction_placement' => 'label',
            'hide_on_screen' => [
                'discussion',
                'comments',
                'format',
                'page_attributes',
                'categories',
                'send-trackbacks',
            ],
            'active' => true,
            'description' => '',
            'show_in_rest' => 0
    ]
    );

//    /* Podcast episode (not course) metadata */
//    acf_add_local_field_group([
//        'key' => 'audiothek_field_group_podcast',
//        'title' => esc_html__(
//            'Episode',
//            'audiothek'
//        ),
//        'fields' => [
//            [
//                'key' => FLD_FORMAT,
//                'label' => esc_html__(
//                    'Format',
//                    'audiothek'
//                ),
//                'name' => FLD_FORMAT,
//                'aria-label' => '',
//                'type' => 'taxonomy',
//                'instructions' => esc_html__(
//                    '"Philosophical Fragments" only.',
//                    'audiothek'
//                ),
//                'required' => 0,
//                'conditional_logic' => 0,
//                'wrapper' => [
//                    'width' => '',
//                    'class' => '',
//                    'id' => '',
//                ],
//                'relevanssi_exclude' => 1,
//                'taxonomy' => TAX_FORMAT,
//                'add_term' => 0,
//                'save_terms' => 1,
//                'load_terms' => 1,
//                'return_format' => 'id',
//                'field_type' => 'select',
//                'allow_null' => 0,
//                'multiple' => 0,
//            ],
//        ],
//        'location' => [
//            [
//                [
//                    'param' => 'post_type',
//                    'operator' => '==',
//                    'value' => SSP_CPT_PODCAST,
//                ],
//            ]
//        ],
//        'menu_order' => 0,
//        'position' => 'side',
//        'style' => 'seamless',
//        'label_placement' => 'left',
//        'instruction_placement' => 'label',
//        'hide_on_screen' => [
//            'discussion',
//            'comments',
//            'format',
//            'page_attributes',
//            'categories',
//            'send-trackbacks',
//        ],
//        'active' => true,
//        'description' => '',
//        'show_in_rest' => 0
//    ]);


    /* Series metadata. */
    acf_add_local_field_group([
        'key' => 'audiothek_field_group_series',
        'title' => esc_html__('Series', 'audiothek'),
        'fields' => [
            [
                'key' => FLD_TAGS,
                'label' => esc_html__(
                    'Keywords',
                    'audiothek'
                ),
                'name' => FLD_TAGS,
                'aria-label' => '',
                'type' => 'taxonomy',
                'instructions' => esc_html__(
                    'Use existing keywords if they are close enough.',
                    'audiothek'
                ),
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'relevanssi_exclude' => 0,
                'taxonomy' => 'post_tag',
                'add_term' => 1,
                'save_terms' => 1,
                'load_terms' => 1,
                'return_format' => 'id',
                'field_type' => 'multi_select',
                'allow_null' => 1,
                'multiple' => 0,
            ],
        ],
        'location' => [
            [
                [
                    'param' => 'taxonomy',
                    'operator' => '==',
                    'value' => ssp_series_slug()
                ],
            ],
            [
                [
                    'param' => 'taxonomy',
                    'operator' => '==',
                    'value' => TAX_COURSE
                ]
            ],
        ],
        'menu_order' => 0,
        'position' => 'side',
        'style' => 'seamless',
        'label_placement' => 'left',
        'instruction_placement' => 'field',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0
    ]);


    /* Course metadata. */
    acf_add_local_field_group([
        'key' => 'audiothek_field_group_course',
        'title' => esc_html__('Course', 'audiothek'),
        'fields' => [
            [
                'key' => FLD_SEMESTER,
                'label' => esc_html__(
                    'Semester',
                    'audiothek'
                ),
                'name' => FLD_SEMESTER,
                'aria-label' => '',
                'type' => 'taxonomy',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'relevanssi_exclude' => 0,
                'taxonomy' => TAX_SEMESTER,
                'add_term' => 1,
                'save_terms' => 1,
                'load_terms' => 1,
                'return_format' => 'id',
                'field_type' => 'select',
                'allow_null' => 0,
                'multiple' => 0,
            ],
            [
                'key' => FLD_COURSE_ID,
                'label' => esc_html__(
                    'Course ID',
                    'audiothek'
                ),
                'name' => FLD_COURSE_ID,
                'aria-label' => '',
                'type' => 'text',
                'instructions' => esc_html__(
                    'E.g., "180123" or "180123-1".',
                    'audiothek'
                ),
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'relevanssi_exclude' => 0,
                'default_value' => '',
                'maxlength' => 9,
                'placeholder' => '180123',
                'prepend' => '',
                'append' => '',
            ],
            [
                'key' => FLD_IMAGE_ID,
                'label' => esc_html__(
                    'Image',
                    'audiothek'
                ),
                'name' => FLD_IMAGE_ID,
                'aria-label' => '',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'relevanssi_exclude' => 1,
                'return_format' => 'id',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
                'preview_size' => 'medium',
            ]
        ],
        'location' => [
            [
                [
                    'param' => 'taxonomy',
                    'operator' => '==',
                    'value' => TAX_COURSE
                ]
            ]
        ],
        'menu_order' => 0,
        'position' => 'side',
        'style' => 'seamless',
        'label_placement' => 'left',
        'instruction_placement' => 'field',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0
    ]);


    /* Licensing metadata. */
    acf_add_local_field_group([
        'key' => 'audiothek_field_group_license',
        'title' => esc_html__('Copyright', 'audiothek'),
        'fields' => [
            [
                'key' => FLD_INHERIT_LICENSE,
                'name' => FLD_INHERIT_LICENSE,
                'label' => esc_html__('Use series license?'),
                'type' => 'true_false',
                'relevanssi_exclude' => true,
                'default_value' => 1
            ],
            [
                'key' => FLD_RIGHTS,
                'name' => FLD_RIGHTS,
                'label' => esc_html__('Third party rights'),
                'type' => 'select',
                'return_format' => 'value',
                'choices' => [
                    /* 'none' is used internally by ACF. */
                    'nil' => esc_html__('None', 'audiothek'),
                    'share' => esc_html__('Share work', 'audiothek'),
                    'derive' => esc_html__('Derive works', 'audiothek')
                ],
                'default_value' => 'share',
                'conditional_logic' => [
                    [
                        [
                            'field' => FLD_INHERIT_LICENSE,
                            'operator' => '!=',
                            'value' => 1
                        ]
                    ]
                ],
                'multiple' => 0,
                'allow_null' => 0
            ],
            [
                'key' => FLD_COMMERCIAL_USE,
                'name' => FLD_COMMERCIAL_USE,
                'label' => esc_html__('Allow commercial use?'),
                'type' => 'true_false',
                'default_value' => 0,
                'conditional_logic' => [
                    [
                        [
                            'field' => FLD_INHERIT_LICENSE,
                            'operator' => '!=',
                            'value' => 1
                        ],
                        [
                            'field' => FLD_RIGHTS,
                            'operator' => '!=',
                            'value' => 'nil'
                        ]
                    ]
                ],
            ],
        ],
        'location' => [
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => SSP_CPT_PODCAST,
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => CPT_LESSON,
                ],
            ],
            [
                [
                    'param' => 'taxonomy',
                    'operator' => '==',
                    'value' => TAX_COURSE
                ]
            ],
            [
                [
                    'param' => 'taxonomy',
                    'operator' => '==',
                    'value' => TAX_SERIES
                ]
            ]
        ],
        'menu_order' => 0,
        'position' => 'side',
        'style' => 'seamless',
        'label_placement' => 'left',
        'instruction_placement' => 'field',
        'active' => true,
    ]);
}

