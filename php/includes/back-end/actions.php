<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek;

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Actions
 */

/* Load plugin JavaScript. */
add_action(
    hook_name: 'admin_enqueue_scripts',
    callback: function (string $hook): void {
        $name = preg_replace('/\.php$/Di', '', $hook, 1);
        $script = 'js' . DIRECTORY_SEPARATOR . $name . '.js';
        if (file_exists(BASE_DIR . DIRECTORY_SEPARATOR . $script)) {
            wp_enqueue_script(
                'audiothek-' . $name,
                BASE_URL . $script
            );
        }
    }
);

/* Load plugin stylesheets. */
add_action(
    hook_name: 'admin_enqueue_scripts',
    callback: function (string $hook): void {
        wp_enqueue_style( 'audiothek-admin', BASE_URL . 'css/admin.css');
    }
);

/*
 * The Audiothek uses ACF to generate custom fields, including the field
 * for selecting the course a lesson belongs to. Hence, an ACF filter
 * is used to count lessons automagically. However, that filter is run
 * BEFORE the lesson is linked to the course it belongs to, to the effect
 * that the counter would NOT be incremented. To fix this, the filter below
 * links lessons to their courses right after the lesson has been saved,
 * which is BEFORE the ACF filter runs.
 */
add_action(
    hook_name: 'save_post_' . CPT_LESSON,
    callback: function (int $post_id, \WP_Post $post, bool $update): void {
        if (isset($_POST['acf'])) {
            $acf = $_POST['acf'];
            if ($acf[FLD_COURSE]) {
                $course = $acf[FLD_COURSE];
                $term_id = (int) sanitize_text_field($course);
                if ($term_id !== 0) {
                    wp_set_object_terms($post_id, $term_id, TAX_COURSE, false);
                }
            }
        }
    },
    accepted_args: 3
);
