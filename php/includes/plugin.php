<?php

/*
 * Boilerplate
 */

declare(strict_types=1);

namespace Univie\Audiothek\Plugin;

use Univie\Audiothek\Errors;

use const Univie\Audiothek\{BASE_DIR, INCLUDE_DIR, PLUGIN_FILE};

if (!defined('\\ABSPATH')) {
    exit();
}


/*
 * Constants
 */

/* Directory to load .mo-files from. */
const LANG_DIR = BASE_DIR . 'lang' . DIRECTORY_SEPARATOR;

/* Directory for files to include in the back-end. */
const BACK_END_DIR = INCLUDE_DIR . 'back-end' . DIRECTORY_SEPARATOR;

/* Directory for files to include in the front-end. */
const FRONT_END_DIR = INCLUDE_DIR . 'front-end' . DIRECTORY_SEPARATOR;

/* Directory for files to include either way. */
const COMMON_DIR = INCLUDE_DIR . 'both-ends' . DIRECTORY_SEPARATOR;


/*
 * Functions
 */

/* Activate the plugin. */
function activate(?bool $throw = null): void
{
    $throw ??= defined('\\WP_DEBUG') && \WP_DEBUG;

    try {
        init(throw: true);
    } catch (Errors\Init $error) {
        if ($throw) {
            throw $error;
        }

        $error->notify();
        deactivate_plugins(PLUGIN_FILE);
        return;
    }

    flush_rewrite_rules(false);
}

/* Initialise the plugin. */
function init(?bool $throw = null): void
{
    $throw ??= defined('\\WP_DEBUG') && \WP_DEBUG;

    try {
        $locale = get_locale();
        $mofile = LANG_DIR . substr($locale, 0, 5) . '.mo';
        load_textdomain('audiothek', $mofile, $locale);

        require COMMON_DIR . 'actions.php';
        require COMMON_DIR . 'api.php';
        require COMMON_DIR . 'constants.php';
        require COMMON_DIR . 'filters.php';
        require COMMON_DIR . 'functions.php';
        require COMMON_DIR . 'post-types.php';
        require COMMON_DIR . 'taxonomies.php';

        if (is_admin()) {
            require BACK_END_DIR . 'fields.php';
            require BACK_END_DIR . 'actions.php';
            require BACK_END_DIR . 'filters.php';
        } else {
            require FRONT_END_DIR . 'actions.php';
            require FRONT_END_DIR . 'filters.php';
        }
    } catch (\Exception $e) {
        $error = new Errors\Init($e);

        if ($throw) {
            throw $error;
        }

        $error->log();
    }
}
