============
Coding Style
============

Code follows the `PER Coding Style v2.0`_ (PER style) and the `WordPress
PHP Coding Standards`_ (WordPress standards). These styles conflict;
where they do, PER style takes precedence over WordPress standards.
However, in order for method invocations to be consistent with
foreign code, method names are in snake_case (WordPress standard),
*not* CamelCase (PER style).

_`PER Coding Style v2.0`: https://www.php-fig.org/per/coding-style/

_`WordPress PHP Coding Standards`: https://developer.wordpress.org/coding-standards/wordpress-coding-standards/php/
